def call(String workspaceDir){  

    def DC_PROJECT="dependency-check scan: /scan"
    def srcVolume = "${workspaceDir}:/scan"
    def reportsVolume = "${workspaceDir}/report:/report"
    def dataVolume = "${workspaceDir}/data:/data"
    def imageName = "owasp/dependency-check:latest"
    def src = "/scan"
    def format = "ALL"
    def reportPath = "${workspaceDir}"
    
    sh 'mkdir -p report'
    sh 'chmod 777 report/'

    sh 'mkdir -p data'
    sh 'chmod 777 data/'
    sh 'ls -la'

    sh "docker run --rm --volume $srcVolume --volume $reportsVolume --volume $dataVolume $imageName --scan $src --format $format --project $DC_PROJECT --out /report"
    sh 'cat report/dependency-check-report.json'
 
}

