def call() {
    sh 'ls -la'

    sh 'curl -sSL https://cli.kondukto.io | sh'

    //sh 'kdt --version'

    sh "mkdir -p \$HOME/bin/ && mv ./kdt-linux-amd64 \$HOME/bin/kdt"

    sh "chmod +x $HOME/bin"

    sh "alias $HOME/bin/kdt=kdt"
                
    sh "$HOME/bin/kdt -v"

    sh "kdt -v"

}
