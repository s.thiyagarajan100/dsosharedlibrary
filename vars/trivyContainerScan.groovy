def call(String imageName, String workspaceDir ){  
  
def sockVolume = "${workspaceDir}/run/docker.sock:/var/run/docker.sock"
def trivyVolume = "${workspaceDir}/output:/output"

sh "docker images"

sh "docker run --rm -v $trivyVolume aquasec/trivy:0.42.1 image $imageName -f json -o /output/results.json"

sh "cat ${workspaceDir}/output/results.json"
 
}
