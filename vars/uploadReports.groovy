def call(String gitURL, String projectName = "", String branchName = "test", String filePath = "sca.json", String scanner = "OSV"){
    echo "Project name is ${projectName}"
    if(projectName==null || projectName.isEmpty())
    {
        projectName = gitURL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
    }
    echo "Project name is ${projectName}"
    echo "gitURL name is ${gitURL}"
    echo "branch name is ${branchName}"
    echo "filePath name is ${filePath}"
     echo "Scanner name is ${scanner}"

      // echo "KONDUKTO_HOST = ${env.KONDUKTO_HOST}"
      // sh 'echo KONDUKTO_HOST = $KONDUKTO_HOST'

    echo "KONDUKTO_HOST = ${env.KONDUKTO_HOST}"
    //sh "echo KONDUKTO_HOST = $KONDUKTO_HOST"
               
    echo "KONDUKTO_TOKEN = ${env.KONDUKTO_TOKEN}"
    //sh "echo KONDUKTO_TOKEN = $KONDUKTO_TOKEN"
    sh 'kdt list scanners --insecure'

if(scanner == "OSV")
{   
    sh "kdt scan -p $projectName -b $branchName -t osvscannersca -f $filePath --insecure"
}
else if (scanner == "DC")
{
    sh "kdt scan -p $projectName -b $branchName -t dependencycheck -f $filePath --insecure"
}


}
